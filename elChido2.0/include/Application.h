#ifndef __APPLICATION_H__
#define __APPLICATION_H__

#include <iostream>
#include "Object3D.h"
#include "Plane.h"
#include <stack>
#include "GLFW\glfw3.h"
#include <vector>
class Matriz3 {
public:
	float matx[3][3];
	static Matriz3 mult(const Matriz3 & m1, const Matriz3 & m2)
	{

		Matriz3 m;
		m.matx[0][0] = m1.matx[0][0] * m2.matx[0][0] + (m1.matx[0][1] * m2.matx[1][0]) + (m1.matx[0][2] * m2.matx[2][0]);
		m.matx[0][1] = m1.matx[0][0] * m2.matx[0][1] + (m1.matx[0][1] * m2.matx[1][1]) + (m1.matx[0][2] * m2.matx[2][1]);
		m.matx[0][2] = m1.matx[0][0] * m2.matx[0][2] + (m1.matx[0][1] * m2.matx[1][2]) + (m1.matx[0][2] * m2.matx[2][2]);

		m.matx[1][0] = m1.matx[1][0] * m2.matx[0][0] + (m1.matx[1][1] * m2.matx[1][0]) + (m1.matx[1][2] * m2.matx[2][0]);
		m.matx[1][1] = m1.matx[1][0] * m2.matx[0][1] + (m1.matx[1][1] * m2.matx[1][1]) + (m1.matx[1][2] * m2.matx[2][1]);
		m.matx[1][2] = m1.matx[1][0] * m2.matx[0][2] + (m1.matx[1][1] * m2.matx[1][2]) + (m1.matx[1][2] * m2.matx[2][2]);

		m.matx[2][0] = m1.matx[2][0] * m2.matx[0][0] + (m1.matx[2][1] * m2.matx[1][0]) + (m1.matx[2][2] * m2.matx[2][0]);
		m.matx[2][1] = m1.matx[2][0] * m2.matx[0][1] + (m1.matx[2][1] * m2.matx[1][1]) + (m1.matx[2][2] * m2.matx[2][1]);
		m.matx[2][2] = m1.matx[2][0] * m2.matx[0][2] + (m1.matx[2][1] * m2.matx[1][2]) + (m1.matx[2][2] * m2.matx[2][2]);

		return m;

	}
	static Matriz3 rotate(const float & gulo) {
		Matriz3 a;
		a.matx[0][0] = cos(gulo);
		a.matx[0][1] = -sin(gulo);
		a.matx[0][2] = 0;

		a.matx[1][0] = sin(gulo);
		a.matx[1][1] = cos(gulo);
		a.matx[1][2] = 0;

		a.matx[2][0] = 0;
		a.matx[2][1] = 0;
		a.matx[2][2] = 1;
		return a;
	}
	Matriz3 tras(const float & x, const float & y) {
		Matriz3 t;
		t.matx[0][0] = 1;
		t.matx[0][1] = 0;
		t.matx[0][2] = x;

		t.matx[1][0] = 0;
		t.matx[1][1] = 1;
		t.matx[1][2] = y;

		t.matx[2][0] = 0;
		t.matx[2][1] = 0;
		t.matx[2][2] = 1;
		return t;

	}
};
class vec3 {
public:

	static vec3 multi(const vec3 & vec, const Matriz3 & m3) {
		vec3 v;
		v.x = vec.x * m3.matx[0][0] + vec.y * m3.matx[0][1] + vec.z * m3.matx[0][2];
		v.y = vec.x * m3.matx[1][0] + vec.y * m3.matx[1][1] + vec.z * m3.matx[1][2];
		v.z = vec.x * m3.matx[2][0] + vec.y * m3.matx[2][1] + vec.z * m3.matx[2][2];
		return v;
	}

	float   x,
		y,
		z;

};

class Application {
public:
	Matriz3 trans(float a);
	std::vector<vec3> mv;
	static const int WIDTH = 512;
	static const int HEIGHT = 512;
	static const int RGB = 3;
	static const int RGBA = 4;
	Application();
	~Application();

	int crearHash(int dx, int dy);
	void init();
	vec3 puntoMedio(vec3 A, vec3 B);
	void rupen(vec3 A, vec3 B, vec3 C, int sub);
	int R, G, B, A;
	void display();
	void reshape(int w, int h);
	void keyboard(int key, int scancode, int action, int mods);
	void update();
	void swapbuffers();
	void cursor_position(double xpos, double ypos);
	void putPixel(GLubyte *buffer, int x, int y, unsigned char R, unsigned char G, unsigned char B, unsigned char A);
	void putPixel(int x, int y, unsigned char R, unsigned char G, unsigned char B, unsigned char A);
	void putPixel(int x, int y);
	void draw();
	void linea(int x0, int y0, int x, int y);
	void moveTo(int x, int y);
	void lineTo(int x, int y);
	void setColor(int _r, int _g, int _b, int _a);
	void setUp();
	void putFigura(const int lado, int radio);
	void clearScreen();

	std::stack<glm::mat4> mStack;
	glm::mat4 mProjectionMatrix, mTransform;
	glm::vec3 vEye;

	GLFWwindow* window;
	unsigned char Buffers[2][WIDTH*HEIGHT*RGBA];
	void putLine(int  x, int  y, int  x1, int  y1);
private:
	GLuint texturesID[2], pboID[2];
	GLuint shaderID;
	GLuint VAO, VBO;
	GLuint sampler;
	GLuint uTransform;
	GLubyte* _screenBuffer;
	int _currentBuffer,
		_nextBuffer;

	glm::vec3 myLightPosition;
	GLuint uMyLightPosition[2];

	Plane oPlane;
	float fTime;
	void initTextures();
	void processPBO();
	void initPBOs();
	void updatePixels(GLubyte *buffer);
	int _drawMode,
		shader;
	bool moveLight;
	int r, g, b, a;
	float rad, angulo;
	int				cx,
		cy;
	void fakeBuffers();
};

#endif //__APPLICATION_H__