#include "Application.h"
#include <vector>
#include <iostream>

std::vector<vec3> vertices;

void Application::update()
{
	Matriz3 tras1, tras2;
	tras1 = tras1.tras(-275, -275);
	tras2 = tras2.tras(255, 255);
	Matriz3 final;
	
	angulo = angulo + 1 % 360;
	rad += angulo * 3.1416 / 180.0f;
	Matriz3 tras, rot, mtx,finalV;
	tras = rot.rotate(rad);
	vec3 v1, v2;
	final = final.mult( tras,tras1 );
	finalV = finalV.mult(tras2, final);
	vertices.clear();
	for (vec3 &v :mv) {
		v1 = v1.multi(v,finalV);
		vertices.push_back(v1);
	}
}

void Application::draw()
{
	clearScreen();
	setColor(255, 255, 255, 0);
	for (int i = 0; i <vertices.size(); i += 3) {
		moveTo(vertices[i].x, vertices[i].y);
		lineTo(vertices[i + 1].x, vertices[i + 1].y);
		moveTo(vertices[i + 1].x, vertices[i + 1].y);
		lineTo(vertices[i + 2].x, vertices[i + 2].y);
		moveTo(vertices[i + 2].x, vertices[i + 2].y);
		lineTo(vertices[i].x, vertices[i].y);
	}

}

void Application::setUp()
{
	moveTo(255, 255);
	putFigura(3, 100);
	rupen(mv.at(0), mv.at(1), mv.at(2), 3);

}
